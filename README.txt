-- SUMMARY --

This Module will create (OrgChart)organisation hierarchy,
you can easily see who is reporting to whom as wll as  you can serach user
by serch functionality implemneted by this module. 


-- BEFORE INSTALLATION --

1) First Download Entity reference module.

-- AFTER INSTALLATION --
1) We have Created 3 field on user level 
  i)LABEL = First Name , MACHINE NAME = field_first_name , FIELD TYPE = Text.
  ii)LABEL = Last Name , MACHINE NAME = field_last_name , FIELD TYPE = Text.
  iii)LABEL = Reporting , MACHINE NAME = field_reporting , FIELD TYPE = Entity Reference.

3)In Entity reference settings we have selected Target type to User.
4)We have created seven uses programmatically for example purpose only. (who is reporting whom)


CONFIGURATION
-------------
 * Enter the top user uid in configuration form , from which hierarchy start , then submit and save
  i)Menu Link for this configuration form is (admin/config/org_hierarchy) or you can find in configuration tab


DEPENDENCY
-------------
 * Download Entity Reference Module
 * Jquery.orgchart.js and Jquery.orgchart.css but this is included in module only



For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/hardik_patel_12/2792759

For a full description of Orgchart visit following pages:
  https://github.com/dabeng/OrgChart
  https://www.jquerycards.com/uncategorised/orgchart/

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/user


-- REQUIREMENTS --

-- INSTALLATION --
* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONTACT --

Current maintainers:
* Hardik Patel - https://www.drupal.org/user/3316709/

