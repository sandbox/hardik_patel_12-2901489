/**
 * @file
 *
 * Orgchart Core Js.
 */


/*
 * jQuery OrgChart Plugin
 * https://github.com/dabeng/OrgChart
 *
 * Demos of jQuery OrgChart Plugin
 * http://dabeng.github.io/OrgChart/local-datasource/
 * http://dabeng.github.io/OrgChart/ajax-datasource/
 * http://dabeng.github.io/OrgChart/ondemand-loading-data/
 * http://dabeng.github.io/OrgChart/option-createnode/
 * http://dabeng.github.io/OrgChart/export-orgchart/
 * http://dabeng.github.io/OrgChart/integrate-map/
 *
 * Copyright 2016, dabeng
 * http://dabeng.github.io/
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
'use strict';

(function(factory) {
  if (typeof module === 'object' && typeof module.exports === 'object') {
    factory(require('jquery'), window, document);
  } else {
    factory(jQuery, window, document);
  }
}(function($, window, document, undefined) {
  $.fn.orgchart = function(options) {
    var defaultOptions = {
      'nodeTitle': 'name',
      'nodeId': 'id',
      'toggleSiblingsResp': false,
      'depth': 999,
      'chartClass': '',
      'exportbutton': false,
      'exportFilename': 'OrgChart',
      'parentNodeSymbol': 'fa-users',
      'draggable': false,
      'direction': 't2b',
      'pan': false,
      'zoom': false
    };

    switch (options) {
      case 'buildhierarchy':
        return buildhierarchy.apply(this, Array.prototype.splice.call(arguments, 1));
      case 'addchildren':
        return addchildren.apply(this, Array.prototype.splice.call(arguments, 1));
      case 'addparent':
        return addparent.apply(this, Array.prototype.splice.call(arguments, 1));
      case 'addsiblings':
        return addsiblings.apply(this, Array.prototype.splice.call(arguments, 1));
      case 'removenodes':
        return removenodes.apply(this, Array.prototype.splice.call(arguments, 1));
      case 'gethierarchy':
        return gethierarchy.apply(this, Array.prototype.splice.call(arguments, 1));
      default: // initiation time
        var opts = $.extend(defaultOptions, options);
    }

    // build the org-chart
    var $chartcontainer = this;
    var data = opts.data;
    var $chart = $('<div>', {
      'data': { 'options': opts },
      'class': 'orgchart' + (opts.chartClass !== '' ? ' ' + opts.chartClass : '') + (opts.direction !== 't2b' ? ' ' + opts.direction : ''),
      'click': function(event) {
        if (!$(event.target).closest('.node').length) {
          $chart.find('.node.focused').removeClass('focused');
        }
      }
    });
    if ($.type(data) === 'object') {
      if (data instanceof $) { // ul datasource
        buildhierarchy($chart, buildjsonds(data.children()), 0, opts);
      } else { // local json datasource
        buildhierarchy($chart, opts.ajaxURL ? data : attachrel(data, '00'), 0, opts);
      }
    } else {
      $.ajax({
        'url': data,
        'dataType': 'json',
        'beforeSend': function () {
          $chart.append('<i class="fa fa-circle-o-notch fa-spin spinner"></i>');
        }
      })
      .done(function(data, textStatus, jqXHR) {
        buildhierarchy($chart, opts.ajaxURL ? data : attachrel(data, '00'), 0, opts);
      })
      .fail(function(jqXHR, textStatus, errorThrown) {
        console.log(errorThrown);
      })
      .always(function() {
        $chart.children('.spinner').remove();
      });
    }
    $chartcontainer.append($chart);

    // append the export button
    if (opts.exportbutton && !$chartcontainer.find('.oc-export-btn').length) {
      var $exportbtn = $('<button>', {
        'class': 'oc-export-btn' + (opts.chartClass !== '' ? ' ' + opts.chartClass : ''),
        'text': 'Export',
        'click': function() {
          if ($(this).children('.spinner').length) {
            return false;
          }
          var $mask = $chartcontainer.find('.mask');
          if (!$mask.length) {
            $chartcontainer.append('<div class="mask"><i class="fa fa-circle-o-notch fa-spin spinner"></i></div>');
          } else {
            $mask.removeClass('hidden');
          }
          var sourcechart = $chartcontainer.addClass('canvasContainer').find('.orgchart:visible').get(0);
          var flag = opts.direction === 'l2r' || opts.direction === 'r2l';
          html2canvas(sourcechart, {
            'width': flag ? sourcechart.clientHeight : sourcechart.clientWidth,
            'height': flag ? sourcechart.clientWidth : sourcechart.clientHeight,
            'onclone': function(cloneDoc) {
              $(cloneDoc).find('.canvasContainer').css('overflow', 'visible')
                .find('.orgchart:visible:first').css('transform', '');
            },
            'onrendered': function(canvas) {
              $chartcontainer.find('.mask').addClass('hidden')
                .end().find('.oc-download-btn').attr('href', canvas.toDataURL())[0].click();
            }
          })
          .then(function() {
            $chartcontainer.removeClass('canvasContainer');
          }, function() {
            $chartcontainer.removeClass('canvasContainer');
          });
        }
      });
      var downloadBtn = '<a class="oc-download-btn' + (opts.chartClass !== '' ? ' ' + opts.chartClass : '') + '"'
        + ' download="' + opts.exportFilename + '.png"></a>';
      $chartcontainer.append($exportbtn).append(downloadBtn);
    }

    if (opts.pan) {
      $chartcontainer.css('overflow', 'hidden');
      $chart.on('mousedown',function(e){
        var $this = $(this);
        if ($(e.target).closest('.node').length) {
          $this.data('panning', false);
          return;
        } else {
          $this.css('cursor', 'move').data('panning', true);
        }
        var lastX = 0;
        var lastY = 0;
        var lastTf = $this.css('transform');
        if (lastTf !== 'none') {
          var temp = lastTf.split(',');
          if (lastTf.indexOf('3d') === -1) {
            lastX = parseInt(temp[4]);
            lastY = parseInt(temp[5]);
          } else {
            lastX = parseInt(temp[12]);
            lastY = parseInt(temp[13]);
          }
        }
        var startX = e.pageX - lastX;
        var startY = e.pageY - lastY;

        $(document).on('mousemove',function(ev) {
          var newX = ev.pageX - startX;
          var newY = ev.pageY - startY;
          var lastTf = $this.css('transform');
          if (lastTf === 'none') {
            if (lastTf.indexOf('3d') === -1) {
              $this.css('transform', 'matrix(1, 0, 0, 1, ' + newX + ', ' + newY + ')');
            } else {
              $this.css('transform', 'matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, ' + newX + ', ' + newY + ', 0, 1)');
            }
          } else {
            var matrix = lastTf.split(',');
            if (lastTf.indexOf('3d') === -1) {
              matrix[4] = ' ' + newX;
              matrix[5] = ' ' + newY + ')';
            } else {
              matrix[12] = ' ' + newX;
              matrix[13] = ' ' + newY;
            }
            $this.css('transform', matrix.join(','));
          }
        });
      });
      $(document).on('mouseup',function() {
        if ($chart.data('panning')) {
          $chart.css('cursor', 'default');
          $(this).off('mousemove');
        }
      });
    }

    if (opts.zoom) {
      $chartcontainer.on('wheel', function(event) {
        event.preventDefault();
        var lastTf = $chart.css('transform');
        var newScale  = 1 + (event.originalEvent.deltaY > 0 ? -0.02 : 0.02);
        if (lastTf === 'none') {
          $chart.css('transform', 'scale(' + newScale + ',' + newScale + ')');
        } else {
          if (lastTf.indexOf('3d') === -1) {
            $chart.css('transform', lastTf + ' scale(' + newScale + ',' + newScale + ')');
          } else {
            $chart.css('transform', lastTf + ' scale3d(' + newScale + ',' + newScale + ', 1)');
          }
        }
      });
    }

    return $chartcontainer;
  };

  function buildjsonds($li) {
    var subObj = {
      'name': $li.contents().eq(0).text().trim(),
      'relationship': ($li.parent().parent().is('li') ? '1': '0') + ($li.siblings('li').length ? 1: 0) + ($li.children('ul').length ? 1 : 0)
    };
    if ($li[0].id) {
      subObj.id = $li[0].id;
    }
    $li.children('ul').children().each(function() {
      if (!subObj.children) { subObj.children = []; }
      subObj.children.push(buildjsonds($(this)));
    });
    return subObj;
  }

  function attachrel(data, flags) {
    data.relationship = flags + (data.children && data.children.length > 0 ? 1 : 0);
    if (data.children) {
      data.children.forEach(function(item) {
        attachrel(item, '1' + (data.children.length > 1 ? 1 : 0));
      });
    }
    return data;
  }

  function loopchart($chart) {
    var $tr = $chart.find('tr:first');
    var subObj = { 'id': $tr.find('.node')[0].id };
    $tr.siblings(':last').children().each(function() {
      if (!subObj.children) { subObj.children = []; }
      subObj.children.push(loopchart($(this)));
    });
    return subObj;
  }

  function gethierarchy($chart) {
    var $chart = $chart || $(this).find('.orgchart');
    if (!$chart.find('.node:first')[0].id) {
      return 'Error: Nodes of orghcart to be exported must have id attribute!';
    }
    return loopchart($chart);
  }

  // detect the exist/display state of related node
  function getnodestate($node, relation) {
    var $target = {};
    if (relation === 'parent') {
      $target = $node.closest('table').closest('tr').siblings(':first').find('.node');
    } else if (relation === 'children') {
      $target = $node.closest('tr').siblings();
    } else {
      $target = $node.closest('table').parent().siblings();
    }
    if ($target.length) {
      if ($target.is(':visible')) {
        return {"exist": true, "visible": true};
      }
      return {"exist": true, "visible": false};
    }
    return {"exist": false, "visible": false};
  }

  // recursively hide the ancestor node and sibling nodes of the specified node
  function hideancestorssiblings($node) {
    var $temp = $node.closest('table').closest('tr').siblings();
    if ($temp.eq(0).find('.spinner').length) {
      $node.closest('.orgchart').data('inAjax', false);
    }
    // hide the sibling nodes
    if (getnodestate($node, 'siblings').visible) {
      hidesiblings($node);
    }
    // hide the lines
    var $lines = $temp.slice(1);
    $lines.css('visibility', 'hidden');
    // hide the superior nodes with transition
    var $parent = $temp.eq(0).find('.node');
    var grandfatherVisible = getnodestate($parent, 'parent').visible;
    if ($parent.length && $parent.is(':visible')) {
      $parent.addClass('slide slide-down').one('transitionend', function() {
        $parent.removeClass('slide');
        $lines.removeAttr('style');
        $temp.addClass('hidden');
      });
    }
    // if the current node has the parent node, hide it recursively
    if ($parent.length && grandfatherVisible) {
      hideancestorssiblings($parent);
    }
  }

  // show the parent node of the specified node
  function showparent($node) {
    // just show only one superior level
    var $temp = $node.closest('table').closest('tr').siblings().removeClass('hidden');
    // just show only one line
    $temp.eq(2).children().slice(1, -1).addClass('hidden');
    // show parent node with animation
    var parent = $temp.eq(0).find('.node')[0];
    repaint(parent);
    $(parent).addClass('slide').removeClass('slide-down').one('transitionend', function() {
      $(parent).removeClass('slide');
      if (isinaction($node)) {
        switchverticalarrow($node.children('.topedge'));
      }
    });
  }

  // recursively hide the descendant nodes of the specified node
  function hidedescendants($node) {
    var $temp = $node.closest('tr').siblings();
    if ($temp.last().find('.spinner').length) {
      $node.closest('.orgchart').data('inAjax', false);
    }
    var $visiblenodes = $temp.last().find('.node:visible');
    var isVerticalDesc = $temp.last().is('.verticalnodes') ? true : false;
    if (!isVerticalDesc) {
      var $lines = $visiblenodes.closest('table').closest('tr').prevAll('.lines').css('visibility', 'hidden');
    }
    $visiblenodes.addClass('slide slide-up').eq(0).one('transitionend', function() {
      $visiblenodes.removeClass('slide');
      if (isVerticalDesc) {
        $temp.addClass('hidden');
      } else {
        $lines.removeAttr('style').addClass('hidden').siblings('.nodes').addClass('hidden');
        $temp.last().find('.verticalnodes').addClass('hidden');
      }
      if (isinaction($node)) {
        switchverticalarrow($node.children('.bottomedge'));
      }
    });
  }

  // show the children nodes of the specified node
  function showdescendants($node) {
    var $temp = $node.closest('tr').siblings();
    var isVerticalDesc = $temp.is('.verticalnodes') ? true : false;
    var $descendants = isVerticalDesc
      ? $temp.removeClass('hidden').find('.node:visible')
      : $temp.removeClass('hidden').eq(2).children().find('tr:first').find('.node:visible');
    // the two following statements are used to enforce browser to repaint
    repaint($descendants.get(0));
    $descendants.addClass('slide').removeClass('slide-up').eq(0).one('transitionend', function() {
      $descendants.removeClass('slide');
      if (isinaction($node)) {
        switchverticalarrow($node.children('.bottomedge'));
      }
    });
  }

  // hide the sibling nodes of the specified node
  function hidesiblings($node, direction) {
    var $nodecontainer = $node.closest('table').parent();
    if ($nodecontainer.siblings().find('.spinner').length) {
      $node.closest('.orgchart').data('inAjax', false);
    }
    if (direction) {
      if (direction === 'left') {
        $nodecontainer.prevAll().find('.node:visible').addClass('slide slide-right');
      } else {
        $nodecontainer.nextAll().find('.node:visible').addClass('slide slide-left');
      }
    } else {
      $nodecontainer.prevAll().find('.node:visible').addClass('slide slide-right');
      $nodecontainer.nextAll().find('.node:visible').addClass('slide slide-left');
    }
    var $animatednodes = $nodecontainer.siblings().find('.slide');
    var $lines = $animatednodes.closest('.nodes').prevAll('.lines').css('visibility', 'hidden');
    $animatednodes.eq(0).one('transitionend', function() {
      $lines.removeAttr('style');
      var $siblings = direction ? (direction === 'left' ? $nodecontainer.prevAll(':not(.hidden)') : $nodecontainer.nextAll(':not(.hidden)')) : $nodecontainer.siblings();
      $nodecontainer.closest('.nodes').prev().children(':not(.hidden)')
        .slice(1, direction ? $siblings.length * 2 + 1 : -1).addClass('hidden');
      $animatednodes.removeClass('slide');
      $siblings.find('.node:visible:gt(0)').removeClass('slide-left slide-right').addClass('slide-up')
        .end().find('.lines, .nodes, .verticalnodes').addClass('hidden')
        .end().addClass('hidden');

      if (isinaction($node)) {
        switchhorizontalarrow($node);
      }
    });
  }

  // show the sibling nodes of the specified node
  function showsiblings($node, direction) {
    // firstly, show the sibling td tags
    var $siblings = $();
    if (direction) {
      if (direction === 'left') {
        $siblings = $node.closest('table').parent().prevAll().removeClass('hidden');
      } else {
        $siblings = $node.closest('table').parent().nextAll().removeClass('hidden');
      }
    } else {
      $siblings = $node.closest('table').parent().siblings().removeClass('hidden');
    }
    // secondly, show the lines
    var $upperlevel = $node.closest('table').closest('tr').siblings();
    if (direction) {
      $upperlevel.eq(2).children('.hidden').slice(0, $siblings.length * 2).removeClass('hidden');
    } else {
      $upperlevel.eq(2).children('.hidden').removeClass('hidden');
    }
    // thirdly, do some cleaning stuff
    if (!getnodestate($node, 'parent').visible) {
      $upperlevel.removeClass('hidden');
      var parent = $upperlevel.find('.node')[0];
      repaint(parent);
      $(parent).addClass('slide').removeClass('slide-down').one('transitionend', function() {
        $(this).removeClass('slide');
      });
    }
    // lastly, show the sibling nodes with animation
    $siblings.find('.node:visible').addClass('slide').removeClass('slide-left slide-right').eq(-1).one('transitionend', function() {
      $siblings.find('.node:visible').removeClass('slide');
      if (isinaction($node)) {
        switchhorizontalarrow($node);
        $node.children('.topedge').removeClass('fa-chevron-up').addClass('fa-chevron-down');
      }
    });
  }

  // start up loading status for requesting new nodes
  function startloading($arrow, $node, options) {
    var $chart = $node.closest('.orgchart');
    if (typeof $chart.data('inAjax') !== 'undefined' && $chart.data('inAjax') === true) {
      return false;
    }

    $arrow.addClass('hidden');
    $node.append('<i class="fa fa-circle-o-notch fa-spin spinner"></i>');
    $node.children().not('.spinner').css('opacity', 0.2);
    $chart.data('inAjax', true);
    $('.oc-export-btn' + (options.chartClass !== '' ? '.' + options.chartClass : '')).prop('disabled', true);
    return true;
  }

  // terminate loading status for requesting new nodes
  function endloading($arrow, $node, options) {
    var $chart = $node.closest('div.orgchart');
    $arrow.removeClass('hidden');
    $node.find('.spinner').remove();
    $node.children().removeAttr('style');
    $chart.data('inAjax', false);
    $('.oc-export-btn' + (options.chartClass !== '' ? '.' + options.chartClass : '')).prop('disabled', false);
  }

  // whether the cursor is hovering over the node
  function isinaction($node) {
    return $node.children('.edge').attr('class').indexOf('fa-') > -1 ? true : false;
  }

  function switchverticalarrow($arrow) {
    $arrow.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
  }

  function switchhorizontalarrow($node) {
    var opts = $node.closest('.orgchart').data('options');
    if (opts.toggleSiblingsResp && (typeof opts.ajaxURL === 'undefined' || $node.closest('.nodes').data('siblingsLoaded'))) {
      var $prevsib = $node.closest('table').parent().prev();
      if ($prevsib.length) {
        if ($prevsib.is('.hidden')) {
          $node.children('.leftedge').addClass('fa-chevron-left').removeClass('fa-chevron-right');
        } else {
          $node.children('.leftedge').addClass('fa-chevron-right').removeClass('fa-chevron-left');
        }
      }
      var $nextsib = $node.closest('table').parent().next();
      if ($nextsib.length) {
        if ($nextsib.is('.hidden')) {
          $node.children('.rightedge').addClass('fa-chevron-right').removeClass('fa-chevron-left');
        } else {
          $node.children('.rightedge').addClass('fa-chevron-left').removeClass('fa-chevron-right');
        }
      }
    } else {
      var $sibs = $node.closest('table').parent().siblings();
      var sibsVisible = $sibs.length ? !$sibs.is('.hidden') : false;
      $node.children('.leftedge').toggleClass('fa-chevron-right', sibsVisible).toggleClass('fa-chevron-left', !sibsVisible);
      $node.children('.rightedge').toggleClass('fa-chevron-left', sibsVisible).toggleClass('fa-chevron-right', !sibsVisible);
    }
  }

  function repaint(node) {
    node.style.offsetWidth = node.offsetWidth;
  }

  // create node
  function createnode(nodedata, level, opts) {
    var dtd = $.Deferred();
    // construct the content of node
    var $nodediv = $('<div' + (opts.draggable ? ' draggable="true"' : '') + (nodedata[opts.nodeId] ? ' id="' + nodedata[opts.nodeId] + '"' : '') + '>')
      .addClass('node ' + (nodedata.className || '') +  (level >= opts.depth ? ' slide-up' : ''))
      .append('<div class="title">' + nodedata[opts.nodeTitle] + '</div>')
      .append(typeof opts.nodeContent !== 'undefined' ? '<div class="content">' + (nodedata[opts.nodeContent] || '') + '</div>' : '');
    // append 4 direction arrows or expand/collapse buttons
    var flags = nodedata.relationship || '';
    if (opts.verticalDepth && (level + 2) > opts.verticalDepth) {
      if ((level + 1) >= opts.verticalDepth && Number(flags.substr(2,1))) {
        $nodediv.append('<i class="toggleBtn fa fa-minus-square"></i>');
      }
    } else {
      if (Number(flags.substr(0,1))) {
        $nodediv.append('<i class="edge verticalEdge topedge fa"></i>');
      }
      if(Number(flags.substr(1,1))) {
        $nodediv.append('<i class="edge horizontalEdge rightedge fa"></i>' +
          '<i class="edge horizontalEdge leftedge fa"></i>');
      }
      if(Number(flags.substr(2,1))) {
        $nodediv.append('<i class="edge verticalEdge bottomedge fa"></i>')
          .children('.title').prepend('<i class="fa '+ opts.parentNodeSymbol + ' symbol"></i>');
      }
    }

    $nodediv.on('mouseenter mouseleave', function(event) {
      var $node = $(this), flag = false;
      var $topedge = $node.children('.topedge');
      var $rightedge = $node.children('.rightedge');
      var $bottomedge = $node.children('.bottomedge');
      var $leftedge = $node.children('.leftedge');
      if (event.type === 'mouseenter') {
        if ($topedge.length) {
          flag = getnodestate($node, 'parent').visible;
          $topedge.toggleClass('fa-chevron-up', !flag).toggleClass('fa-chevron-down', flag);
        }
        if ($bottomedge.length) {
          flag = getnodestate($node, 'children').visible;
          $bottomedge.toggleClass('fa-chevron-down', !flag).toggleClass('fa-chevron-up', flag);
        }
        if ($leftedge.length) {
          switchhorizontalarrow($node);
        }
      } else {
        $node.children('.edge').removeClass('fa-chevron-up fa-chevron-down fa-chevron-right fa-chevron-left');
      }
    });

    // define click event handler
    $nodediv.on('click', function(event) {
      $(this).closest('.orgchart').find('.focused').removeClass('focused');
      $(this).addClass('focused');
    });

    // define click event handler for the top edge
    $nodediv.on('click', '.topedge', function(event) {
      var $that = $(this);
      var $node = $that.parent();
      var parentState = getnodestate($node, 'parent');
      if (parentState.exist) {
        var $parent = $node.closest('table').closest('tr').siblings(':first').find('.node');
        if ($parent.is('.slide')) { return; }
        // hide the ancestor nodes and sibling nodes of the specified node
        if (parentState.visible) {
          hideancestorssiblings($node);
          $parent.one('transitionend', function() {
            if (isinaction($node)) {
              switchverticalarrow($that);
              switchhorizontalarrow($node);
            }
          });
        } else { // show the ancestors and siblings
          showparent($node);
        }
      } else {
        // load the new parent node of the specified node by ajax request
        var nodeId = $that.parent()[0].id;
        // start up loading status
        if (startloading($that, $node, opts)) {
        // load new nodes
          $.ajax({ 'url': opts.ajaxURL.parent + nodeId + '/', 'dataType': 'json' })
          .done(function(data) {
            if ($node.closest('.orgchart').data('inAjax')) {
              if (!$.isEmptyObject(data)) {
                addparent.call($node.closest('.orgchart').parent(), $node, data, opts);
              }
            }
          })
          .fail(function() { console.log('Failed to get parent node data'); })
          .always(function() { endloading($that, $node, opts); });
        }
      }
    });

    // bind click event handler for the bottom edge
    $nodediv.on('click', '.bottomedge', function(event) {
      var $that = $(this);
      var $node = $that.parent();
      var childrenState = getnodestate($node, 'children');
      if (childrenState.exist) {
        var $children = $node.closest('tr').siblings(':last');
        if ($children.find('.node:visible').is('.slide')) { return; }
        // hide the descendant nodes of the specified node
        if (childrenState.visible) {
          hidedescendants($node);
        } else { // show the descendants
          showdescendants($node);
        }
      } else { // load the new children nodes of the specified node by ajax request
        var nodeId = $that.parent()[0].id;
        if (startloading($that, $node, opts)) {
          $.ajax({ 'url': opts.ajaxURL.children + nodeId + '/', 'dataType': 'json' })
          .done(function(data, textStatus, jqXHR) {
            if ($node.closest('.orgchart').data('inAjax')) {
              if (data.children.length) {
                addchildren($node, data, $.extend({}, opts, { depth: 0 }));
              }
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('Failed to get children nodes data');
          })
          .always(function() {
            endloading($that, $node, opts);
          });
        }
      }
    });

    // event handler for toggle buttons in Hybrid(horizontal + vertical) OrgChart
    $nodediv.on('click', '.toggleBtn', function(event) {
      var $this = $(this);
      var $descwrapper = $this.parent().next();
      var $descendants = $descwrapper.find('.node');
      var $children = $descwrapper.children().children('.node');
      if ($children.is('.slide')) { return; }
      $this.toggleClass('fa-plus-square fa-minus-square');
      if ($descendants.eq(0).is('.slide-up')) {
        $descwrapper.removeClass('hidden');
        repaint($children.get(0));
        $children.addClass('slide').removeClass('slide-up').eq(0).one('transitionend', function() {
          $children.removeClass('slide');
        });
      } else {
        $descendants.addClass('slide slide-up').eq(0).one('transitionend', function() {
          $descendants.removeClass('slide');
          // $descwrapper.addClass('hidden');
          $descendants.closest('ul').addClass('hidden');
        }).find('.toggleBtn').removeClass('fa-minus-square').addClass('fa-plus-square');
      }
    });

    // bind click event handler for the left and right edges
    $nodediv.on('click', '.leftedge, .rightedge', function(event) {
      var $that = $(this);
      var $node = $that.parent();
      var siblingsState = getnodestate($node, 'siblings');
      if (siblingsState.exist) {
        var $siblings = $node.closest('table').parent().siblings();
        if ($siblings.find('.node:visible').is('.slide')) { return; }
        if (opts.toggleSiblingsResp) {
          var $prevsib = $node.closest('table').parent().prev();
          var $nextsib = $node.closest('table').parent().next();
          if ($that.is('.leftedge')) {
            if ($prevsib.is('.hidden')) {
              showsiblings($node, 'left');
            } else {
              hidesiblings($node, 'left');
            }
          } else {
            if ($nextsib.is('.hidden')) {
              showsiblings($node, 'right');
            } else {
              hidesiblings($node, 'right');
            }
          }
        } else {
          if (siblingsState.visible) {
            hidesiblings($node);
          } else {
            showsiblings($node);
          }
        }
      } else {
        // load the new sibling nodes of the specified node by ajax request
        var nodeId = $that.parent()[0].id;
        var url = (getnodestate($node, 'parent').exist) ? opts.ajaxURL.siblings : opts.ajaxURL.families;
        if (startloading($that, $node, opts)) {
          $.ajax({ 'url': url + nodeId + '/', 'dataType': 'json' })
          .done(function(data, textStatus, jqXHR) {
            if ($node.closest('.orgchart').data('inAjax')) {
              if (data.siblings || data.children) {
                addsiblings($node, data, opts);
              }
            }
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('Failed to get sibling nodes data');
          })
          .always(function() {
            endloading($that, $node, opts);
          });
        }
      }
    });
    if (opts.draggable) {
      $nodediv.on('dragstart', function(event) {
        if (/firefox/.test(window.navigator.userAgent.toLowerCase())) {
          event.originalEvent.dataTransfer.setData('text/html', 'hack for firefox');
        }
        var $dragged = $(this);
        var $dragzone = $dragged.closest('.nodes').siblings().eq(0).find('.node:first');
        var $draghier = $dragged.closest('table').find('.node');
        $dragged.closest('.orgchart')
          .data('dragged', $dragged)
          .find('.node').each(function(index, node) {
            if ($draghier.index(node) === -1) {
              if (opts.dropcriteria) {
                if (opts.dropcriteria($dragged, $dragzone, $(node))) {
                  $(node).addClass('allowedDrop');
                }
              } else {
                $(node).addClass('allowedDrop');
              }
            }
          });
      })
      .on('dragover', function(event) {
        event.preventDefault();
        var $dropzone = $(this);
        var $dragged = $dropzone.closest('.orgchart').data('dragged');
        var $dragzone = $dragged.closest('.nodes').siblings().eq(0).find('.node:first');
        if ($dragged.closest('table').find('.node').index($dropzone) > -1 ||
          (opts.dropcriteria && !opts.dropcriteria($dragged, $dragzone, $dropzone))) {
          event.originalEvent.dataTransfer.dropEffect = 'none';
        }
      })
      .on('dragend', function(event) {
        $(this).closest('.orgchart').find('.allowedDrop').removeClass('allowedDrop');
      })
      .on('drop', function(event) {
        var $dropzone = $(this);
        var $orgchart = $dropzone.closest('.orgchart');
        var $dragged = $orgchart.data('dragged');
        $orgchart.find('.allowedDrop').removeClass('allowedDrop');
        var $dragzone = $dragged.closest('.nodes').siblings().eq(0).children();
        // firstly, deal with the hierarchy of drop zone
        if (!$dropzone.closest('tr').siblings().length) { // if the drop zone is a leaf node
          $dropzone.append('<i class="edge verticalEdge bottomedge fa"></i>')
            .parent().attr('colspan', 2)
            .parent().after('<tr class="lines"><td colspan="2"><div class="down"></div></td></tr>'
            + '<tr class="lines"><td class="right">&nbsp;</td><td class="left">&nbsp;</td></tr>'
            + '<tr class="nodes"></tr>')
            .siblings(':last').append($dragged.find('.horizontalEdge').remove().end().closest('table').parent());
        } else {
          var dropColspan = parseInt($dropzone.parent().attr('colspan')) + 2;
          var horizontalEdges = '<i class="edge horizontalEdge rightedge fa"></i><i class="edge horizontalEdge leftedge fa"></i>';
          $dropzone.closest('tr').next().addBack().children().attr('colspan', dropColspan);
          if (!$dragged.find('.horizontalEdge').length) {
            $dragged.append(horizontalEdges);
          }
          $dropzone.closest('tr').siblings().eq(1).children(':last').before('<td class="left top">&nbsp;</td><td class="right top">&nbsp;</td>')
            .end().next().append($dragged.closest('table').parent());
          var $dropsibs = $dragged.closest('table').parent().siblings().find('.node:first');
          if ($dropsibs.length === 1) {
            $dropsibs.append(horizontalEdges);
          }
        }
        // secondly, deal with the hierarchy of dragged node
        var dragColspan = parseInt($dragzone.attr('colspan'));
        if (dragColspan > 2) {
          $dragzone.attr('colspan', dragColspan - 2)
            .parent().next().children().attr('colspan', dragColspan - 2)
            .end().next().children().slice(1, 3).remove();
          var $dragsibs = $dragzone.parent().siblings('.nodes').children().find('.node:first');
          if ($dragsibs.length ===1) {
            $dragsibs.find('.horizontalEdge').remove();
          }
        } else {
          $dragzone.removeAttr('colspan')
            .find('.bottomedge').remove()
            .end().end().siblings().remove();
        }
        $orgchart.triggerHandler({ 'type': 'nodedropped.orgchart', 'draggedNode': $dragged, 'dragzone': $dragzone.children(), 'dropzone': $dropzone });
      });
    }
    // allow user to append dom modification after finishing node create of orgchart 
    if (opts.createnode) {
      opts.createnode($nodediv, nodedata);
    }
    dtd.resolve($nodediv);
    return dtd.promise();
  }
  // recursively build the tree
  function buildhierarchy ($appendto, nodedata, level, opts, callback) {
    var $nodewrapper;
    // Construct the node
    var $childnodes = nodedata.children;
    var haschildren = $childnodes ? $childnodes.length : false;
    var isVerticalNode = (opts.verticalDepth && (level + 1) >= opts.verticalDepth) ? true : false;
    if (Object.keys(nodedata).length > 1) { // if nodedata has nested structure
      $nodewrapper = isVerticalNode ? $appendto : $('<table>');
      if (!isVerticalNode) {
        $appendto.append($nodewrapper);
      }
      $.when(createnode(nodedata, level, opts))
      .done(function($nodediv) {
        if (isVerticalNode) {
          $nodewrapper.append($nodediv);
        }else {
          $nodewrapper.append($nodediv.wrap('<tr><td' + (haschildren ? ' colspan="' + $childnodes.length * 2 + '"' : '') + '></td></tr>').closest('tr'));
        }
        if (callback) {
          callback();
        }
      })
      .fail(function() {
        console.log('Failed to creat node')
      });
    }
    // Construct the inferior nodes and connectiong lines
    if (haschildren) {
      if (Object.keys(nodedata).length === 1) { // if nodedata is just an array
        $nodewrapper = $appendto;
      }
      var isHidden = level + 1 >= opts.depth ? ' hidden' : '';
      var isverticalLayer = (opts.verticalDepth && (level + 2) >= opts.verticalDepth) ? true : false;

      // draw the line close to parent node
      if (!isverticalLayer) {
        $nodewrapper.append('<tr class="lines' + isHidden + '"><td colspan="' + $childnodes.length * 2 + '"><div class="down"></div></td></tr>');
      }
      // draw the lines close to children nodes
      var lineLayer = '<tr class="lines' + isHidden + '"><td class="right">&nbsp;</td>';
      for (var i=1; i<$childnodes.length; i++) {
        lineLayer += '<td class="left top">&nbsp;</td><td class="right top">&nbsp;</td>';
      }
      lineLayer += '<td class="left">&nbsp;</td></tr>';
      var $nodelayer;
      if (isverticalLayer) {
        $nodelayer = $('<ul>');
        if (level + 2 === opts.verticalDepth) {
          $nodewrapper.append('<tr class="verticalnodes"><td></td></tr>')
            .find('.verticalnodes').children().append($nodelayer);
        } else {
          $nodewrapper.append($nodelayer);
        }
      } else {
        $nodelayer = $('<tr class="nodes' + isHidden + '">');
        $nodewrapper.append(lineLayer).append($nodelayer);
      }
      // recurse through children nodes
      $.each($childnodes, function() {
        var $nodecell = isverticalLayer ? $('<li>') : $('<td colspan="2">');
        $nodelayer.append($nodecell);
        buildhierarchy($nodecell, this, level + 1, opts, callback);
      });
    }
  }

  // build the child nodes of specific node
  function buildchildnode ($appendto, nodedata, opts, callback) {
    var opts = opts || $appendto.closest('.orgchart').data('options');
    var data = nodedata.children || nodedata.siblings;
    $appendto.find('td:first').attr('colspan', data.length * 2);
    buildhierarchy($appendto, { 'children': data }, 0, opts, callback);
  }
  // exposed method
  function addchildren($node, data, opts) {
    var count = 0;
    buildchildnode.call($node.closest('.orgchart').parent(), $node.closest('table'), data, opts, function() {
      if (++count === data.children.length) {
        if (!$node.children('.bottomedge').length) {
          $node.append('<i class="edge verticalEdge bottomedge fa"></i>');
        }
        if (!$node.find('.symbol').length) {
          $node.children('.title').prepend('<i class="fa '+ opts.parentNodeSymbol + ' symbol"></i>');
        }
        showdescendants($node);
      }
    });
  }

  // build the parent node of specific node
  function buildparentnode($currentroot, nodedata, opts, callback) {
    var that = this;
    var $table = $('<table>');
    nodedata.relationship = '001';
    $.when(createnode(nodedata, 0, opts || $currentroot.closest('.orgchart').data('options')))
      .done(function($nodediv) {
        $table.append($nodediv.removeClass('slide-up').addClass('slide-down').wrap('<tr class="hidden"><td colspan="2"></td></tr>').closest('tr'));
        $table.append('<tr class="lines hidden"><td colspan="2"><div class="down"></div></td></tr>');
        var linesRow = '<td class="right">&nbsp;</td><td class="left">&nbsp;</td>';
        $table.append('<tr class="lines hidden">' + linesRow + '</tr>');
        var $oc = that.children('.orgchart');
        $oc.prepend($table)
          .children('table:first').append('<tr class="nodes"><td colspan="2"></td></tr>')
          .children('tr:last').children().append($oc.children('table').last());
        callback();
      })
      .fail(function() {
        console.log('Failed to create parent node');
      });
  }

  // exposed method
  function addparent($currentroot, data, opts) {
    buildparentnode.call(this, $currentroot, data, opts, function() {
      if (!$currentroot.children('.topedge').length) {
        $currentroot.children('.title').after('<i class="edge verticalEdge topedge fa"></i>');
      }
      showparent($currentroot);
    });
  }

  // subsequent processing of build sibling nodes
  function complementline($onesibling, siblingCount, existingSibligCount) {
    var lines = '';
    for (var i = 0; i < existingSibligCount; i++) {
      lines += '<td class="left top">&nbsp;</td><td class="right top">&nbsp;</td>';
    }
    $onesibling.parent().prevAll('tr:gt(0)').children().attr('colspan', siblingCount * 2)
      .end().next().children(':first').after(lines);
  }

  // build the sibling nodes of specific node
  function buildsiblingnode($nodechart, nodedata, opts, callback) {
    var opts = opts || $nodechart.closest('.orgchart').data('options');
    var newSiblingCount = nodedata.siblings ? nodedata.siblings.length : nodedata.children.length;
    var existingSibligCount = $nodechart.parent().is('td') ? $nodechart.closest('tr').children().length : 1;
    var siblingCount = existingSibligCount + newSiblingCount;
    var insertPostion = (siblingCount > 1) ? Math.floor(siblingCount/2 - 1) : 0;
    // just build the sibling nodes for the specific node
    if ($nodechart.parent().is('td')) {
      var $parent = $nodechart.closest('tr').prevAll('tr:last');
      $nodechart.closest('tr').prevAll('tr:lt(2)').remove();
      var childCount = 0;
      buildchildnode.call($nodechart.closest('.orgchart').parent(),$nodechart.parent().closest('table'), nodedata, opts, function() {
        if (++childCount === newSiblingCount) {
          var $siblingtds = $nodechart.parent().closest('table').children('tr:last').children('td');
          if (existingSibligCount > 1) {
            complementline($siblingtds.eq(0).before($nodechart.closest('td').siblings().addBack().unwrap()), siblingCount, existingSibligCount);
            $siblingtds.addClass('hidden').find('.node').addClass('slide-left');
          } else {
            complementline($siblingtds.eq(insertPostion).after($nodechart.closest('td').unwrap()), siblingCount, 1);
            $siblingtds.not(':eq(' + insertPostion + 1 + ')').addClass('hidden')
              .slice(0, insertPostion).find('.node').addClass('slide-right')
              .end().end().slice(insertPostion).find('.node').addClass('slide-left');
          }
          callback();
        }
      });
    } else { // build the sibling nodes and parent node for the specific ndoe
      var nodeCount = 0;
      buildhierarchy($nodechart.closest('.orgchart'), nodedata, 0, opts, function() {
        if (++nodeCount === siblingCount) {
          complementline($nodechart.next().children('tr:last')
            .children().eq(insertPostion).after($('<td colspan="2">')
            .append($nodechart)), siblingCount, 1);
          $nodechart.closest('tr').siblings().eq(0).addClass('hidden').find('.node').addClass('slide-down');
          $nodechart.parent().siblings().addClass('hidden')
            .slice(0, insertPostion).find('.node').addClass('slide-right')
            .end().end().slice(insertPostion).find('.node').addClass('slide-left');
          callback();
        }
      });
    }
  }

  function addsiblings($node, data, opts) {
    buildsiblingnode.call($node.closest('.orgchart').parent(), $node.closest('table'), data, opts, function() {
      $node.closest('.nodes').data('siblingsLoaded', true);
      if (!$node.children('.leftedge').length) {
        $node.children('.topedge').after('<i class="edge horizontalEdge rightedge fa"></i><i class="edge horizontalEdge leftedge fa"></i>');
      }
      showsiblings($node);
    });
  }

  function removenodes($node) {
    var $parent = $node.closest('table').parent();
    var $sibs = $parent.parent().siblings();
    if ($parent.is('td')) {
      if (getnodestate($node, 'siblings').exist) {
        $sibs.eq(2).children('.top:lt(2)').remove();
        $sibs.eq(':lt(2)').children().attr('colspan', $sibs.eq(2).children().length);
        $parent.remove();
      } else {
        $sibs.eq(0).children().removeAttr('colspan')
          .find('.bottomedge').remove()
          .end().end().siblings().remove();
      }
    } else {
      $parent.add($parent.siblings()).remove();
    }
  }

}));
