/**
 * @file
 *
 * Chart-container Hierarchy.
 */

'use strict';
(function ($) {
    Drupal.behaviors.idestination_handler = {
        attach: function (context, settings) {
            var datascource = Drupal.settings.orgchart.datascource;
            $('#chart-container').orgchart({
                'data': datascource,
                'depth': 3,
                'pan': true,
                'zoom': true,
                'nodeContent': 'title',
                'nodeID': 'id',
            });
            var baseUrl = window.location.pathname;
            var x = 0;
            jQuery('div .node').each(function (i, obj) { // all div shown to page which has class (node)
                var id = jQuery(this).find(".fa-users").parent().parent().attr('id'); // find id of parent user
                    jQuery("#" + id).children('.title , .content').click(function () { // when parent user is clicked
                        var url = baseUrl + '?user=' + id;
                        jQuery(location).attr('href', url);
                    })
            });
            jQuery('#back-btn').click(function () {
                history.go(-1);
                return false;
            });


        }
    };

})(jQuery);




